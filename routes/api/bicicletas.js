var express = require('express'); //express tiene el modulo de router
var router = express.Router();
var bicicletacontroller = require('../../controllers/api/biciControllerAPI'); //requerimos controlador

router.get('/', bicicletacontroller.Bicicleta_list);

router.post('/create', bicicletacontroller.Bicicleta_create);

router.delete('/delete', bicicletacontroller.Bicicleta_delete);

module.exports = router;