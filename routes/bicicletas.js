var express = require('express'); //express tiene el modulo de router
var router = express.Router();
var bicicletacontroller = require('../controllers/bicicleta'); //requerimos controlador

router.get('/', bicicletacontroller.bicicleta_list);

router.get('/create', bicicletacontroller.bicicleta_create_get);

router.post('/create', bicicletacontroller.bicicleta_create_post);

router.post('/:id/delete', bicicletacontroller.bicicleta_delete_post);  //Con ':'  indico que le estoy pasando un parametro. En este caso id

module.exports = router;
