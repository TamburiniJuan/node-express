//Require para importar el modelo
var Bicicleta = require('../models/bicicleta');

//Lista a renderizar segun archivo pug
exports.bicicleta_list = function (req, res){
    res.render('bicicletas/bicis', {bicis: Bicicleta.allBicis});
}

exports.bicicleta_create_get = function(req, res){ // GET SE UTILIZA PARA SOLICITAR DATOS DE MANERA NO DESTRUCTIVA
	res.render('bicicletas/create'); //crear vista con formulario
}

exports.bicicleta_create_post = function (req, res){ // POST SE UTILIZARA PARA SOLICITAR DATOS DESTRUCTIVOS, QUE MODIFICARON LA INFO DEL SERVIDOR
	var bici = new Bicicleta (req.body.id, req.body.color, req.body.modelo);
	bici.ubicacion = [req.body.lat, req.body.lng];
	Bicicleta.add(bici);

	res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function(req, res){
	Bicicleta.removeById(req.body.id);

	res.redirect('/bicicletas');
}