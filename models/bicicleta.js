//Declaracion de la bicicleta y sus propiedades
var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = function (){
    return 'id: ' + this.id + '| color: ' + this.color;
}

//Array AllBicis y funcion de push
Bicicleta.allBicis = [];
Bicicleta.add = function(abici){
    Bicicleta.allBicis.push(abici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find()
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`); // Interpolacion solo con comilla francesa    
}

Bicicleta.removeById = function(aBiciId) {
    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id = aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}


//Objeto Bicicleta
var a = new Bicicleta(1, 'rojo', 'urbano', [-34.607408, -58.410734]);
var b = new Bicicleta(2, 'negro', 'urbano', [-34.606331, -58.405874]);

//Se agregan al array
Bicicleta.add(a);
Bicicleta.add(b);

//Se exporta el modulo
module.exports = Bicicleta;